package com.bioinviertetec.paratodos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View
import android.widget.EditText;
import android.widget.Toast;

import com.bioinviertetec.paratodos.R;

public class MainActivity extends AppCompatActivity {

    public EditText txtObjeto;
    public EditText txtDescripción;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        txtObjeto = (EditText)findViewById(R.id.txt_Objeto);
        txtDescripción = (EditText)findViewById(R.id.txt_Descripción);

        public void onClickButton(View view){
            String Objeto = txtObjeto.getText().toString();
            String Descripción = txtDescripción.getText().toString();

            String msg = "Objeto: " + Objeto + "Descripción: " + Descripción;
            Toast.makeText(this, "msg", Toast.LENGTH_SHORT).show();
        }
    }
}
